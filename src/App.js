import React, { Component } from 'react';
import { View } from 'react-native';
import firebase from 'firebase';

import { Header, CardSection, Button, Spinner } from './components/common';
import LoginForm from './components/LoginForm';

export default class App extends Component {
  state = { loggedIn: null };

  componentWillMount() {
    this.initFirebase();
    this.handleAuthState();
  }

  initFirebase() {
    firebase.initializeApp({
      apiKey: 'AIzaSyCsHezHBEwzOt4wjmax_vYb35RCNGNWLX8',
      authDomain: 'authentication-d8f83.firebaseapp.com',
      databaseURL: 'https://authentication-d8f83.firebaseio.com',
      storageBucket: 'authentication-d8f83.appspot.com',
      messagingSenderId: '842346454753'
    });
  }

  handleAuthState() {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.setState({ loggedIn: true });
      } else {
        this.setState({ loggedIn: false });
      }
    });
  }

  renderContent() {
    switch (this.state.loggedIn) {
      case true:
        return (
          <CardSection>
            <Button onPress={() => firebase.auth().signOut()}>
              Log Out
            </Button>
          </CardSection>
        );
      case false:
        return <LoginForm />;
      default:
        return (
          <CardSection>
            <Spinner />
          </CardSection>
        );
    }
  }

  render() {
    return (
      <View>
        <Header headerText={'Authentication'} />
        {this.renderContent()}
      </View>
    );
  }
}
